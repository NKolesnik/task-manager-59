package ru.t1consulting.nkolesnik.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectTaskEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.ITaskEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.listener.AbstractListener;
import ru.t1consulting.nkolesnik.tm.util.DateUtil;

import java.util.List;

@Getter
@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    public ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    public IProjectTaskEndpoint projectTaskEndpoint;

    protected void renderTasks(@NotNull final List<TaskDto> tasks) {
        int index = 1;
        for (TaskDto task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@Nullable final TaskDto task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

package ru.t1consulting.nkolesnik.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDto;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserRegistryRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserRegistryResponse;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

@Component
public class UserRegistryListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "registry";

    @NotNull
    public static final String DESCRIPTION = "Registry new user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL]");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @NotNull final UserRegistryResponse response = getUserEndpoint().registryUser(request);
        @Nullable UserDto user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}

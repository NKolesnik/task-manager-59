package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {

    List<ProjectDto> findAll(@Nullable Sort sort);

    List<ProjectDto> findAll(@Nullable String userId, @Nullable Sort sort);

    List<ProjectDto> findAll(@Nullable Comparator comparator);

    List<ProjectDto> findAll(@Nullable String userId, @Nullable Comparator comparator);

}

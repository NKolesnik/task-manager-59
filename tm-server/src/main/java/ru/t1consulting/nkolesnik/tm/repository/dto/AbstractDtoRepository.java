package ru.t1consulting.nkolesnik.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IDtoRepository;
import ru.t1consulting.nkolesnik.tm.comparator.CreatedComparator;
import ru.t1consulting.nkolesnik.tm.comparator.DateBeginComparator;
import ru.t1consulting.nkolesnik.tm.comparator.NameComparator;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractModelDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;

@Getter
@Repository
public abstract class AbstractDtoRepository<M extends AbstractModelDto> implements IDtoRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void add(@Nullable M modelDto) {
        entityManager.persist(modelDto);
    }

    @Override
    public abstract long getSize();

    @NotNull
    @Override
    public abstract List<M> findAll();

    @Nullable
    @Override
    public abstract M findById(@Nullable String id);

    @Override
    public abstract boolean existsById(@Nullable String id);

    @Override
    public void update(@NotNull M modelDTO) {
        entityManager.merge(modelDTO);
    }

    @Override
    public abstract void clear();

    @Override
    public abstract void remove(@Nullable M modelDTO);

    @Override
    public abstract void removeById(@Nullable String id);

    @Nullable
    protected M getFirstRecord(@Nullable final List<M> resultList) {
        return resultList.size() > 0 ? resultList.get(0) : null;
    }

    protected String getSortColumnName(@Nullable Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else if (comparator == DateBeginComparator.INSTANCE) return "dateBegin";
        else return "status";
    }

}

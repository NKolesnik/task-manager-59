package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDto;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.List;

@Repository
public class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    public UserDto findByLogin(@Nullable final String login) {
        List<UserDto> resultList = entityManager.
                createQuery("SELECT u FROM UserDto u WHERE u.login = :login", UserDto.class).
                setParameter("login", login).
                setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@Nullable final String email) {
        List<UserDto> resultList = entityManager.
                createQuery("SELECT u FROM UserDto u WHERE u.email = :email", UserDto.class).
                setHint("org.hibernate.cacheable", true).
                setParameter("email", email).
                setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        return entityManager.
                createQuery("SELECT COUNT (1) = 1 FROM UserDto u WHERE u.login = :login", Boolean.class).
                setParameter("login", login).
                getSingleResult();
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        return entityManager.
                createQuery("SELECT COUNT (1) = 1 FROM UserDto u WHERE u.email = :email", Boolean.class).
                setParameter("email", email).
                getSingleResult();
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        entityManager.merge(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        entityManager.merge(user);
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) return;
        @Nullable final UserDto user = findById(id);
        if (user == null) return;
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        entityManager.merge(user);
    }

    @Override
    public void setPassword(@Nullable final UserDto user, @Nullable final String password) {
        if (user == null) return;
        if (password == null || password.isEmpty()) return;
        @Nullable final UserDto repositoryUser = findById(user.getId());
        if (repositoryUser == null) return;
        repositoryUser.setPasswordHash(
                HashUtil.salt(password, propertyService.getPasswordSecret(), propertyService.getPasswordIteration())
        );
        entityManager.merge(repositoryUser);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        @Nullable final UserDto user = this.findByLogin(login);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(u) FROM UserDto u", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public List<UserDto> findAll() {
        return entityManager.createQuery("SELECT u FROM UserDto u", UserDto.class).getResultList();
    }

    @Nullable
    @Override
    public UserDto findById(@Nullable final String id) {
        List<UserDto> resultList = entityManager.
                createQuery("SELECT u FROM UserDto u WHERE u.id = :id", UserDto.class).
                setParameter("id", id).
                getResultList();
        return getFirstRecord(resultList);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.contains(id);
    }

    @Override
    public void clear() {
        for (@NotNull final UserDto user : findAll()) {
            entityManager.remove(user);
        }
    }

    @Override
    public void remove(@Nullable final UserDto user) {
        entityManager.remove(user);
    }

    @Override
    public void removeById(@Nullable final String id) {
        @Nullable final UserDto user = this.findById(id);
        if (user == null) return;
        entityManager.remove(user);
    }

}

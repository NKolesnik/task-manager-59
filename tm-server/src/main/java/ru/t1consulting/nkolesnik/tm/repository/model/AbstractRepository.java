package ru.t1consulting.nkolesnik.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.model.IRepository;
import ru.t1consulting.nkolesnik.tm.comparator.CreatedComparator;
import ru.t1consulting.nkolesnik.tm.comparator.DateBeginComparator;
import ru.t1consulting.nkolesnik.tm.comparator.NameComparator;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;

@Getter
@Repository
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void add(@Nullable M model) {
        entityManager.persist(model);
    }

    @Override
    public abstract long getSize();

    @NotNull
    @Override
    public abstract List<M> findAll();

    @Nullable
    @Override
    public abstract M findById(@Nullable String id);

    @Override
    public abstract boolean existsById(@Nullable String id);

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

    @Override
    public abstract void clear();

    @Override
    public abstract void remove(@Nullable M model);

    @Override
    public abstract void removeById(@Nullable String id);

    protected String getSortColumnName(@Nullable Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else if (comparator == DateBeginComparator.INSTANCE) return "dateBegin";
        else return "status";
    }

    @Nullable
    protected M getFirstRecord(@Nullable final List<M> resultList) {
        return resultList.size() > 0 ? resultList.get(0) : null;
    }

}

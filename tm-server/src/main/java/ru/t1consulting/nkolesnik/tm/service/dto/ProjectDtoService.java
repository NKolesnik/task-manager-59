package ru.t1consulting.nkolesnik.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDto, IProjectDtoRepository> implements IProjectDtoService {

    @NotNull
    @Autowired
    protected IProjectDtoRepository repository;

    @Override
    @Transactional
    public void add(@Nullable final ProjectDto project) {
        if (project == null) throw new ProjectNotFoundException();
        repository.add(project);
    }

    @Override
    @Transactional
    public void add(@Nullable final String userId, @Nullable final ProjectDto project) {
        if (project == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.add(userId, project);
    }

    @Override
    @Transactional
    public void add(@Nullable final Collection<ProjectDto> projects) {
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        for (ProjectDto project : projects)
            repository.add(project);

    }

    @Override
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Collection<ProjectDto> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        for (ProjectDto project : projects)
            repository.add(userId, project);
    }

    @Override
    @Transactional
    public void set(@Nullable final Collection<ProjectDto> projects) {
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        repository.clear();
        for (ProjectDto project : projects)
            repository.add(project);
    }

    @Override
    @Transactional
    public void set(@Nullable final String userId, @Nullable final Collection<ProjectDto> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        repository.clear();
        for (ProjectDto project : projects)
            repository.add(userId, project);

    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDto project = new ProjectDto();

        project.setName(name);
        project.setUserId(userId);
        repository.add(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDto project = new ProjectDto();

        project.setName(name);
        project.setUserId(userId);
        project.setDescription(description);
        repository.add(project);
        return project;
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll() {
        @Nullable final List<ProjectDto> projects;
        projects = repository.findAll();
        if (projects.isEmpty()) return Collections.emptyList();
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<ProjectDto> projects;
        projects = repository.findAll(userId);
        if (projects.isEmpty()) return Collections.emptyList();
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@Nullable final Comparator<ProjectDto> comparator) {
        if (comparator == null) return findAll();
        @Nullable final List<ProjectDto> projectList;
        projectList = repository.findAll(comparator);
        if (projectList.isEmpty()) return Collections.emptyList();
        return projectList;
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @Nullable final List<ProjectDto> projectList;
        projectList = repository.findAll(sort);
        if (projectList.isEmpty()) return Collections.emptyList();
        return projectList;

    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId, @Nullable final Comparator<ProjectDto> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @Nullable final List<ProjectDto> projectList;
        projectList = repository.findAll(userId, comparator);
        if (projectList.isEmpty()) return Collections.emptyList();
        return projectList;
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final List<ProjectDto> projectList;
        projectList = repository.findAll(userId, sort);
        if (projectList.isEmpty()) return Collections.emptyList();
        return projectList;
    }

    @Nullable
    @Override
    public ProjectDto findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    public ProjectDto findById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findById(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

    @Override
    @Transactional
    public void update(@Nullable final ProjectDto project) {
        if (project == null) throw new ProjectNotFoundException();
        repository.update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDto project = repository.findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (status == null) throw new StatusNotFoundException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @Nullable final ProjectDto project = repository.findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.update(project);
    }

    @Override
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final ProjectDto project) {
        if (project == null) throw new ProjectNotFoundException();
        repository.remove(project);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final ProjectDto project) {
        if (project == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.remove(userId, project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        repository.removeById(id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeById(userId, id);
    }

}
